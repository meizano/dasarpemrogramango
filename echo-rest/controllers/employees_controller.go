package controllers

import (
	"net/http"
	"echo-rest/handlers"

	"github.com/labstack/echo/v4"
)

//FetchAllCustomers ...
func FetchAllEmployees(c echo.Context) (err error) {

	result, err := handlers.FetchEmployees()

	return c.JSON(http.StatusOK, result)
}

func AddEmployees(c echo.Context) (err error) {

	result, err := handlers.AddEmployee(c)

	return c.JSON(http.StatusOK, result)
}
