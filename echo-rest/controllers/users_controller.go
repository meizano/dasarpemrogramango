package controllers

import (
	"net/http"
	"echo-rest/handlers"

	"github.com/labstack/echo/v4"
)

//AddUser ...
func FetchAllUsers(c echo.Context) (err error) {

	result, err := handlers.FetchUsers()

	return c.JSON(http.StatusOK, result)
}

//StoreCustomer ...
func StoreUser(c echo.Context) (err error) {

	result, err := handlers.StoreUser(c)

	return c.JSON(http.StatusOK, result)
}

//UpdateUser ...
func UpdateUser(c echo.Context) (err error) {

	result, err := handlers.UpdateUser(c)

	return c.JSON(http.StatusOK, result)
}

//DeleteUser ...
func DeleteUser(c echo.Context) (err error) {

	result, err := handlers.DeleteUser(c)

	return c.JSON(http.StatusOK, result)
}
